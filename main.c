#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

//NUMS_TO_GENERATE est le nombre de cartes dans le paquet de cartes,
#define NUMS_TO_GENERATE 52

//vérifie l'unicité de la "carte" number obtenue,
//c'est-à-dire que le nombre entre 0 et 51 nest pas deja dans le tableau
//les arguments sont de la forme check(shuffle, shuffle[i], i)
int check(int *array, int number, int i){
    for (int j = 0; j < i; j++){
        if (number == array[j]){
            number = rand() % NUMS_TO_GENERATE;
            number = check(array, number, i);
        }
    }
    return number;
}

//fait correspondre le nombre avec la carte correspondante, l'affiche et renvoi sa valeur
int value_card(int number) {
    int value = 0;
    switch (number) {
        case 0 ... 3:
            printf("AS");
            value = 11;
            break;
        case 4 ... 7:
            printf("King");
            value = 10;
            break;
        case 8 ... 11:
            printf("Queen");
            value = 10;
            break;
        case 12 ... 15:
            printf("Jack");
            value = 10;
            break;
        case 16 ... 19:
            printf("10");
            value = 10;
            break;
        case 20 ... 23:
            printf("9");
            value = 9;
            break;
        case 24 ... 27:
            printf("8");
            value = 8;
            break;
        case 28 ... 31:
            printf("7");
            value = 7;
            break;
        case 32 ... 35:
            printf("6");
            value = 6;
            break;
        case 36 ...39:
            printf("5");
            value = 5;
            break;
        case 40 ... 43:
            value = 4;
            printf("4");
            break;
        case 44 ... 47:
            value = 3;
            printf("3");
            break;
        case 48 ... 51:
            value = 2;
            printf("2");
            break;
        default : break;

    }
    // affiche "carte   valeur  couleur"
    printf(value == 11 ? "\t %d/1 \t" : "\t %d \t", value);
    switch (number % 4) {
        case 0:
            printf("coeur");
            break;
        case 1:
            printf("carreaux");
            break;
        case 2:
            printf("pique");
            break;
        case 3:
            printf("trefle");
            break;
    }
    printf("\n");
    return value;
}

//donne la valeur du jeu du joueur (ou du croupier)
int sum_value(int *array, int max){
    printf("carte\t value\tcouleur\n");
    int sum = 0;
    int number_aces = 0;
    for (int i = 0; i < max; i++){
        sum += value_card(array[i]);
        if (array[i] <= 3){
            number_aces += 1;
        }
    }
    //resolution du problème de l'AS pouvant avoir une valeur de 11 ou 1
    while (number_aces > 0 && sum > 21){
        sum = sum - 10;
        number_aces -= 1;
    }
    printf("la valeur du jeu est %d\n", sum);
    return sum;
}

//calcul qui gagne, value1 la valeur du jeu du joueur et value2 la valeur du jeu du croupier
// 1 : gagnant, 2 : perdant, 3 : égalité
int result(int value1, int value2){
    if (value1 > 21){
        return 2;
    }
    else if (value1 == value2){
        return 3;
    }
    else if (value2 > 21 || value1 > value2 ) {
        return 1;
    }
    else {
        return 2;
    }
}

int main() {
    printf("Bienvenue, si vous ne connaissez pas les regles du BlackJack, le but est d'avoir une main\n"
           "d'une valeur superieur a celle du croupier mais ne depassant pas 21\n"
           "si vous depassez les 21 ou si vous avez moins que le croupier vous perdez votre mise\n"
           "si vous avez la meme valeur que le croupier, vous recuperez la mise mais ne gagnez rien\n"
           "si vous avez 21 ou moins et plus que le croupier vous gagne un montant de jetons egale a votre mise\n");
    //chips est le nombre de jetons
    int chips;
    printf("Entrer votre nombre de jetons (exemple 100 si premiere partie)\n");
    scanf("%d", &chips);
    while (getchar() != '\n');
    //replay est une variable servant à savoir si l'utilisateur souhaite rejouer
    char replay = 'R';
    while (replay == 'R' || replay == 'r') {
        //shuffle[] le tableau dans lequel le melange de carte sera stocké
        int shuffle[NUMS_TO_GENERATE] = {};
        //player[] le tableau dans lequel les cartes du joueur seront stocké
        int player[10] = {};
        //dealer[] le tableau dans lequel les cartes du croupier seront stocké
        int dealer[10] = {};
        int player_value;
        int dealer_value;

        srand(time(NULL));
        for (int i = 0; i < NUMS_TO_GENERATE; i++) {
            shuffle[i] = rand() % NUMS_TO_GENERATE;
            shuffle[i] = check(shuffle, shuffle[i], i);
        }
        int bet = 0;
        printf("combien voulez vous miser?\n");

        scanf("%d", &bet);
        while (getchar() != '\n');
        while (bet > chips){
            printf("vous n'avez pas assez de jetons, combien voulez vous miser?\n");
            scanf("%d", &bet);
            while (getchar() != '\n');
        }

        //debut du jeu, distribution des cartes
        //card est la variable pour avancer dans le tableau représentant le paquet de cartes
        int card = 0;
        //card_player est la variable pour avancer dans le tableau représentant le jeu du joueur1
        int card_player = 0;
        //card_dealer est la variable pour avancer dans le tableau représentant le jeu du croupier
        int card_dealer = 0;
        //Distribution initiale du jeu
        player[card_player++] = shuffle[card++];
        dealer[card_dealer++] = shuffle[card++];
        player[card_player++] = shuffle[card++];
        printf("les cartes du croupiers sont:\n");
        dealer_value = sum_value(dealer, card_dealer);
        printf("\n");
        printf("vos cartes sont:\n");
        player_value = sum_value(player, card_player);
        //Maintenant interaction utilisateur
        char choice;
        printf("que faire? h/s/d\nh pour Hit, tirer une carte\ns pour Stand, rester avec votre jeux\n"
               "d pour Double, prendre une derniere carte et doubler la mise \n");
        choice = (char) getchar();
        while (getchar() != '\n');
        while (choice == 'H' || choice == 'h') {
            player[card_player++] = shuffle[card++];
            printf("vos cartes\n");
            player_value = sum_value(player, card_player);
            if (player_value > 21) {
                printf("vous avez depasse 21.");
                sleep(1);
                choice = 'S';
            } else {
                printf("Hit/Stand/Double?\n");
                choice = (char) getchar();
                while (getchar() != '\n');
            }
        }
        //le joueur peut doubler sa mise uniquement si ses jetons sont au moins 2 fois supérieur a sa mise
        if (choice == 'D' || choice == 'd') {
            if (chips >= 2 * bet) {
                bet *= 2;
            }
            else {
                printf("vous n'avez pas assez de jetons, vous prenez une derniere carte mais ne doublez pas la mise\n");
                sleep(1);
            }
            player[card_player++] = shuffle[card++];
            printf("vos cartes\n");
            player_value = sum_value(player, card_player);
        }
        //décision du croupier automatique
        while (dealer_value < 17) {
            dealer[card_dealer++] = shuffle[card++];
            printf("\nles cartes du dealer\n");
            dealer_value = sum_value(dealer, card_dealer);
            sleep(1);
        }
        if (result(player_value, dealer_value) == 1) {
            printf("Gagne, vous doublez votre mise");
            chips += bet;
        }
        else if (result(player_value, dealer_value) == 3){
            printf("Egalite, vous gardez votre mise");
        }
        else {
            printf("Perdu, vous perdez votre mise");
            chips -= bet;
        }
        printf("\n");
        printf("vous avez maintenant %d jetons\nRejouer?\nR pour rejouer/Q pour arreter la partie\n", chips);
        replay = (char) getchar();
        while (getchar() != '\n');
        while (chips <= 0) {
            printf("vous n'avez plus de jetons, combien voulez vous en reprendre?\n");
            scanf("%d", &chips);
            while (getchar() != '\n');
        }
    }
    printf("Merci d'avoir jouer. Revenez vous voir\n");
    printf("N'hesitez pas a retenir votre nombre de jetons restant pour votre prochaine partie");
    return 0;
}
